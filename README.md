# PROG1004_Oblig3_Grp46.   Gruppemedlem: Mariam 


Velkommen til prosjektwikien for **PåGodVei** (PGV), et prosjekt utviklet av Gruppe 46 i PROG1004. Her finner du en oversikt over prosjektets mål, rammer, omfang, organisering, valg av utviklingsmetode, risikoanalyse og kvalitetssikring. Jeg har også tatt hensyn til viktige tilbakemeldinger fra læringsassistenten på Oblig 1 og har forsøkt å forbedre vårt prosjekt i tråd med disse kommentarene.


### Bakgrunn

I de kalde vintermånedene i Norge møter lokalsamfunn utfordringer med å håndtere snø og is på veiene. Små kommuner og private eiendomsbesittere opplever ofte vanskeligheter med å få pålitelig snøryddingstjeneste i tide, noe som øker risikoen for ulykker på glatte veier. For å adressere disse problemene har vi utviklet PåGodVei (PGV), en smart digital løsning for effektiv brøyting og strøing av veier.

Investorer vi kan gå etter er: 
* Kommuner eller fylker som ønsker å forbedre trafikkflyten og sikkerheten på veiene.
* Private selskaper innen transport- og logistikkbransjen som ønsker å tilby bedre tjenester til kundene sine.
* Teknologiselskaper med interesse for smarte løsninger innen mobilitet og samferdsel.


## Mål

### Effektmål:

Vi ønsker å fokusere på de langsiktige virkningene av applikasjonen, inkludert økt trafikksikkerhet, redusert risiko for ulykker og forbedret effektivitet av brøyte- og strøetjenester.

+ Sikre trygg ferdsel på vinterveiene etter snøfall, forbedre sikkerheten og redusere risikoen for ulykker på glatte veier.
+ For å gjøre dette mer bærekraftig og miljøvennlig, må vi anvende datainformasjon for å forutse snøfall og veiforhold. Det er viktig å redusere drivstofforbruket og utslippene fra vedlikeholdsarbeidet.
+ Kundene og allmennheten som bruker denne applikasjonen, vil redusere kostnadene ved å effektivisere planleggingen og gjennomføringen av ressursbruken.
+ Målet er å etablere tillit blant brukerne ved å levere pålitelige og nøyaktige oppdateringer om veistatus og trafikkforhold, samt å være lydhør overfor brukerens behov og bekymringer.


### Resultatmål:

De resultatmålene vi ønsker å fokusere på for prosjektet inkluderer utviklingen av en brukervennlig mobilapplikasjon som gir sanntidsoppdateringer om veistatus og trafikkforhold, samt muligheten til å rapportere problemer. 

+ Applikasjonen skal tilby et fleksibelt og effektivt planleggingsverktøy som tilpasser seg skiftende værforhold og prioriterer vedlikehold i kritiske områder.
+ Det er viktig at brøytesjåfører får kontinuerlig oppdatert informasjon om veistatus for å kunne utføre sitt arbeid effektivt.
+ Applikasjonen må være fungerende og tilgjengelig på utgivelsesdatoen når den publiseres.

### Rammer

Prosjektet PåGodVei (PGV) har tydelig definerte rammer som styrer dets omfang, tidslinjer, budsjett og sikkerhetsaspekter. Disse rammene er essensielle for å sikre at prosjektet når sine mål innenfor gitte begrensninger.

+ Tidsramme: Prosjektet ble startet i januar og planlegges avsluttet i juni, en periode på seks måneder. Dette tidsvinduet er valgt for å sikre tilstrekkelig fremdrift og kvalitet i utviklingen av applikasjonen før den lanseres på markedet.

+ Budsjett: Vi har tilgang til et totalbudsjett på 1 million kr for utvikling, testing og markedsføring, som er dekket av SIT. Dette budsjettet vil bli forvaltet nøye for å sikre effektiv bruk av ressurser og oppnå målene innenfor de gitte økonomiske rammene.

+ Datasikkerhet og personvern: Alle data som samles inn og behandles av applikasjonen vil være underlagt strenge sikkerhets- og personvernstandarder. Dette inkluderer beskyttelse av personlige opplysninger og sensitiv informasjon om brukere og deres eiendommer. Tilgjengeligheten av tjenester og data vil også være sikret for å sikre en jevn og pålitelig opplevelse for alle brukere.

Disse rammene vil fungere som retningslinjer for prosjektets gjennomføring og vil bli overvåket og evaluert jevnlig for å sikre at prosjektet forblir innenfor de fastsatte grensene. Ved å følge disse rammebetingelsene vil vi kunne oppnå våre mål på en effektiv og bærekraftig måte.



## Produkt Backlog 

Se også issue board

| ID | Beskrivelse                                      | Prioritet | Sprint  | Status     |
|----|--------------------------------------------------|-----------|---------|------------|
| 1  | Idegenerering                                    | Høy       | Sprint 1| Fullført   |
| 2  | Markedsundersøkelse                             | Høy       | Sprint 1| Pågående  |
| 3  | Kravspesifikasjon                               | Høy       | Sprint 1| Pågående  |
| 4  | Design                                           | Middels   | Sprint 1| Planlagt   |
| 5  | Arkitekturdesign                                 | Middels   | Sprint 1| Planlagt   |
| 6  | Teknologivalg                                    | Middels   | Sprint 1| Planlagt   |
| 7  | Plattformvalg                                    | Middels   | Sprint 1| Planlagt   |
| 8  | Backend-utvikling                                | Høy       | Sprint 2| Planlagt   |
| 9  | Frontend-utvikling                               | Høy       | Sprint 2| Planlagt   |
| 10 | Database-design                                  | Høy       | Sprint 2| Planlagt   |
| 11 | Implementering av nøkkelkarakteristikk           | Middels   | Sprint 2| Planlagt   |
| 12 | Sanntidsoppdateringer                            | Høy       | Sprint 3| Planlagt   |
| 13 | Bestilling av tjenester                          | Høy       | Sprint 3| Planlagt   |
| 14 | Publikumsinformasjon                             | Høy       | Sprint 3| Planlagt   |
| 15 | Rapportering av problemer                        | Middels   | Sprint 3| Planlagt   |
| 16 | Systemtesting                                    | Høy       | Sprint 4| Planlagt   |
| 17 | Brukertesting                                    | Høy       | Sprint 4| Planlagt   |
| 18 | Feilretting                                      | Høy       | Sprint 4| Planlagt   |
| 19 | Universell utforming                             | Middels   | Sprint 4| Planlagt   |



## Prosjektomfang

### Problemområde og avgrensning
Hovedproblemet vi ønsker å løse er behovet for en brukervennlig og pålitelig applikasjon som effektivt hjelper brøytefirmaer, gjør det enkelt for kunder å bestille deres tjenester, og gir publikum den informasjonen de ønsker. Vi vil fokusere spesifikt på å utvikle en løsning for små brøytefirmaer, bedrifter som ønsker å bestille deres tjenester, og publikum som påvirkes av veiforholdene og ønsker å holde seg informert. Vår løsning vil håndtere bestillinger av faste og engangsoppdrag, samt tilby sanntidsstatusoppdateringer og værdata fra meteorologiske institutt.  Vi vil ikke utvikle nye betalingssystemer, men integrere eksisterende betalingsløsninger for eventuelle premiumfunksjoner. Vårt produkt vil skille seg fra konkurrentene ved å tilby en intuitiv og brukervennlig plattform med fokus på sanntidsoppdateringer og brukerinteraksjon.



### Produktvisjon
For dette prosjektet har vi omformulert en kort visjon for produktet: 
> "For små brøytefirmaer, kunder som trenger snøbrøyting og strøing, og den vanlige personen som er avhengig av trygge veiforhold, er PåGodVei en nødvendig ressurs. Vår visjon er å være den ledende plattformen som forenkler og optimaliserer snøbrøyting og strøing, samtidig som vi gir publikum tilgang til sanntidsinformasjon om veistatus. Gjennom å tilby en brukervennlig og innovativ plattform ønsker vi å skape trygghet hos våre brukere, samtidig som vi ivaretar bærekraft ved å minimere unødvendige oppdrag."

I motsetning til eksisterende løsninger, vil PåGodVei skille seg ut med sin brukervennlighet, sanntidsoppdateringer og muligheten for brukere å rapportere inn problemer direkte til brøyteselskaper.

### Personas
Vi har identifisert flere personas som representerer våre hovedbrukere, inkludert brøytesjåføren, kunden som trenger brøyte- og strøtjenester, og publikum som ønsker informasjon om veiforholdene. Disse personasene vil guide utviklingen av PåGodVei for å sikre at løsningen møter deres behov og forventninger.



![Persona 1](Screenshot_2024-04-10_at_21.04.14.png)



![Persona 2](Screenshot_2024-04-10_at_21.04.30.png)



![Persona 3](Screenshot_2024-04-10_at_21.04.45.png)



## Oppgavebeskrivelse

Vår oppgave er å utvikle en digital plattform kalt PåGodVei, som skal gi brukerne sanntidsinformasjon om veistatus og trafikkforhold. Dette vil hjelpe både daglige pendlere og andre veibrukere med å planlegge reiser og unngå farlige situasjoner på veiene. Plattformen skal bestå av en mobilapplikasjon for brukere og et tilhørende webgrensesnitt for administrasjon av informasjon.

### Mobilapplikasjonens funksjonalitet:

+ Brukere skal kunne sjekke veistatus for deres daglige pendleruter, inkludert informasjon om eventuelle trafikkproblemer som isete veier eller snøkaos.
+ Sanntidsoppdateringer om trafikkforhold og brøyteaktiviteter skal være tilgjengelig.
+ Applikasjonen skal tilby en brukervennlig plattform for å rapportere inn problemer og behov direkte til brøyteselskaper.
+ Mulighet for å motta varsler om veiendringer og problemer basert på brukerens valgte ruter.
+ Tilgang til værprognoser og veiforhold i sanntid for å hjelpe brukerne med å planlegge sine reiser.

### Webgrensesnittets funksjonalitet for administrasjon:

+ Mulighet for administratører å legge til og oppdatere informasjon om veistrekninger, trafikkforhold og brøyteaktiviteter.
+ Statistikk over bruk av plattformen, inkludert antall brukere, mest brukte funksjoner og rapporterte problemer.
+ Administrasjon av brukerprofiler og tilgangsrettigheter for ulike roller, for eksempel administratører og moderatorer.
+ Tilrettelegging for å legge til nye funksjoner og forbedringer basert på tilbakemeldinger fra brukerne.




## Prosjektorganisering

#### Organisasjonskart

![Organisasjonskart](Screenshot_2024-04-10_at_18.49.18.png)
Figur - Organisasjonskart og scrum-teamet

### Ansvarsforhold og roller 

**Produkteier**: 
+ Produktansvarlig og representant for kundens interesser.
+ Ansvarlig for å definere og prioritere oppgaver i product backlogen basert på kundens behov og verdien de tilfører produktet.
+ Kommuniserer jevnlig med utviklingsteamet for å sikre at produktet utvikles i tråd med kundens forventninger.
+ Delta i sprint planleggingsmøter for å forklare kravene og sikre felles forståelse av oppgavene.

**Scrum Master - Mariam**
+ Ansvarlig for å fasilitere Scrum-prosessen og sikre at teamet følger metodikken.
+ Lede daglige scrum-møter for å oppdatere teamet om fremgang, identifisere eventuelle hindringer og koordinere løsninger.
+ Hjelpe teamet med å fjerne hindringer som kan påvirke fremdriften.
+ Sørge for at Scrum-prinsippene og rammeverket overholdes, og hjelpe teamet med å kontinuerlig forbedre seg.

**Front-end utvikler - Lars**
+ Ansvarlig for å implementere brukergrensesnittet basert på design og krav fra produkt backlogen.
+ Samarbeide tett med backend-utvikleren for å sikre integrasjonen av frontend og backend-delen av løsningen.
+ Delta i sprint planning-møter for å forstå kravene og estimere oppgavene som skal utføres.

**Back-end utvikler og Android - Heidi**
+ Asvarlig for å implementere backend-løsningen og Android-applikasjonen basert på kravene i product backlogen.
+ Samarbeide med frontend-utvikleren for å sikre at grensesnittet og funksjonaliteten integreres sømløst.
+ Delta i sprint planning-møter for å estimere oppgaver og sikre at løsningen utvikles i henhold til kravene.

**IOS utvikler - Jonatan**
+ Ansvarlig for å utvikle iOS-applikasjonen basert på kravene i product backlogen.
+ Samarbeide med både frontend- og backend-utviklerne for å sikre at iOS-applikasjonen fungerer sømløst med resten av løsningen.
+ Delta i sprint planning-møter for å forstå kravene og bidra til estimering av oppgaver.


## Systemutviklingsmodell (Scrum)

Utviklerne av PåGodVei-applikasjonen er fire nylig utdannede IT-studenter med begrenset erfaring fra lignende prosjekter. Dette prosjektet er vår første programvareutviklingsprosjekt, og som sådan er det viktig å ha en inkrementell utviklingsmetode som kan håndtere endringer effektivt. Vi har valgt å benytte Scrum som vår utviklingsmetode for å adressere disse utfordringene.

**Ulemper:**
+ Endringshåndtering: Vi må være åpne for tilbakemeldinger fra kunder underveis og være villige til å tilpasse oss endringer i funksjoner eller legge til nye funksjoner. En utfordring kan være hvis det kommer for mange endringsforespørsler fra kunder, noe som kan føre til tidstap og manglende fokus.
+ Rolleforståelse: Alle gruppemedlemmer må ha forståelse for den agile modellen og utfordringene med å bestemme budsjett og tidsrammer. Rollen til produktansvarlig er avgjørende, da produktansvarlig er den som representerer kundens behov. Hvis produktansvarlig ikke utfører sin rolle effektivt, kan det påvirke det endelige resultatet negativt.
+ Overholdelse av regler: Scrum krever streng deltakelse og overholdelse av faste regler fra teamet. Dette kan føre til frustrasjon og konflikter i gruppen, spesielt hvis enkelte individer har vanskeligheter med å tilpasse seg metodikken.

**Fordeler:**
+ Fleksibilitet: Scrum er en svært fleksibel metodikk som tillater teamet å tilpasse seg endringer underveis i prosjektet. Dette gjør det mulig å håndtere uforutsette utfordringer og tilpasse seg endrede krav fra kunder.
+ Tidlig tilbakemelding: Scrum legger til rette for tidlig testing og tilbakemelding fra kunder. Dette gjør at eventuelle feil eller misforståelser oppdages tidlig i prosessen, noe som bidrar til å sikre at produktet møter kundens behov.
+ Struktur og produktivitet: Scrum gir teamet en tydelig struktur og plan for hva som skal gjøres. Dette øker produktiviteten og effektiviteten, samtidig som det sikrer høy produktkvalitet. Regelmessige statusmøter og tilbakemeldinger bidrar også til bedre kommunikasjon og samarbeid i teamet.

Basert på analysen av fordeler og ulemper har vårt team valgt å benytte Scrum-utviklingsmodellen for dette prosjektet på grunn av dens fleksibilitet og evne til å tilpasse seg endringer underveis i utviklingsprosessen.


### Planlegging og Gjennomføring

For å sikre en grundig gjennomgang av hvordan vi planlegger å implementere Scrum spesifikt for vårt prosjekt, vil vi utdype følgende punkter:

1. Sprintlengde:
Vi planlegger å ha sprinter med en varighet på to uker. Denne lengden vil gi oss tilstrekkelig tid til å implementere funksjonalitet og samtidig sikre jevn fremdrift gjennom prosjektet.

2. Planlegging av sprinter:
Sprintplanleggingsmøtene vil bli gjennomført før starten av hver sprint. Under disse møtene vil vi velge oppgaver fra product backlogen basert på prioritet og kompleksitet. Oppgavene vil bli estimert, og sprintmålet vil bli definert for å sikre en felles forståelse av hva som skal oppnås i løpet av sprinten.

3. Gjennomføring av sprinter:
De daglige stand-up-møtene vil bli arrangert for å oppdatere hverandre om fremdriften, identifisere eventuelle hindringer og koordinere løsninger. Møtene vil være korte og fokuserte for å maksimere effektiviteten.
Sprint review-møter vil bli gjennomført ved slutten av hver sprint for å demonstrere funksjonaliteten som er utviklet, få tilbakemelding fra interessenter og identifisere eventuelle endringer som må gjøres i product backlogen.

4. Retrospektiver:
Etter hvert sprint vil vi gjennomføre retrospektiver for å evaluere teamets ytelse, identifisere forbedringsområder og implementere endringer for å øke effektiviteten. Dette vil gi oss muligheten til kontinuerlig å tilpasse og forbedre våre arbeidsmetoder.

5. Bruk av verktøy:
Vi planlegger å bruke et digitalt scrum board for å administrere product backlogen og visualisere fremdriften gjennom sprintene. Dette vil hjelpe oss med å holde oversikt over oppgavene og sikre god kommunikasjon og samarbeid i teamet.

**Roller og ansvar:**
+ Mariam vil inneha rollen som Scrum Master og vil være ansvarlig for å fasilitere Scrum-prosessen, fjerne    hindringer og sikre at teamet følger metodikken på en effektiv måte.
+ Produktansvarlig, representert av lokal kommune Gjøvik, vil prioritere oppgaver i product backlogen basert på kundenes behov og markedsundersøkelser.
+ Utviklerne, Lars, Heidi, og Jonatan, vil være ansvarlige for å implementere produktet i henhold til kravene og designretningslinjene som er definert.

Ved å tydeliggjøre disse punktene, sikrer vi en grundig forståelse av hvordan vi planlegger å bruke Scrum-metodikken for å lede prosjektet vårt mot suksess.


## Risikohåndtering

Tabellen under er en risikoanalyse og en plan for risikohåndtering for ulike områder 
innenfor prosjektet. Disse områdene inkluderer teknologi, prosjekt, og forretningsaspekter.
For hvert område indentifiseres spesifikke risikofaktorer, graden av påvirkning og 
sannsynlighet, samt tiltak for å håndtere risikoen effektivt. 

![Risikoanalyse tabell](Screenshot_2024-04-10_at_19.23.14.png)


## Kvalitetssikring

**Grupperegler**
1. Møteeffektivitet: Alle møter skal ha et klart definert formål, en agenda og en tidsramme. Dette sikrer at møtet forblir fokusert og produktivt.
2. Kommunikasjon: Kommunikasjonen skal være tydelig, respektfull og konstruktiv. Dette inkluderer både skriftlig kommunikasjon på plattformer som Microsoft Teams og Discord, samt muntlig kommunikasjon under møter.
3. Oppgavefordeling: Oppgaver skal fordeles jevnt blant teammedlemmene basert på ferdigheter og interesser. Dette sikrer at alle bidrar til prosjektet og føler seg engasjert.

**Kvalitetsdokumentasjon**
1. Prosjektdokumentasjon: All prosjektdokumentasjon, inkludert møtenotater, beslutningsreferater og endringsloggføring, skal være grundig og nøyaktig for å sikre sporbarhet og gjensidig forståelse.
2. Versjonskontroll: En solid versjonskontrollstrategi skal følges for å sikre at alle endringer i prosjektet er sporbare og reversible. Dette innebærer bruk av Git og GitLab for kildekontroll og versjonsstyring.
3. Dokumentasjonsstandarder: Det skal følges fastsatte standarder for dokumentasjon av prosjektet, inkludert formatering, språkbruk og struktur. Dette sikrer en enhetlig og profesjonell presentasjon av prosjektet.


### Verktøybruk

| Nr. | Verktøy             | Beskrivelse                                               |
|-----|---------------------|-----------------------------------------------------------|
| 1   | Microsoft Teams     | For å organisere gruppearbeidet.                         |
| 2   | Slack             | For å kommunisere med gruppemedlemmene, veileder og oppdragsgiver. |
| 3   | Dropbox             | For å lagre dokumenter til prosjektet.                    |
| 4   | Github              | For å styre koden og styre prosjektgruppens arbeid.       |
| 5   | Xcode               | For IOS-utvikling av applikasjonen.                      |
| 6   | Visual Studio Code  | For utvikling av back-end.                          |
| 7   | Trello              | For å administrere Product Backlog.                      |
| 8   | Canva        | For å lage Gantt-diagram.                                |
| 9   | Excel               | For å loggføre arbeidstimer i detaljert.                 |


## Gjennomføring

Gantt diagramm

![Gantt diagramm for prosjektet](Online_Gantt_20240410__1_.png)


## Arkitekturskisse

### Lagdelt Arkitektur

**Lagdelt arkitektur**

![Lagdelt arkitektur](lagdelt-arkitektur.drawio.png)

**Mikroservices Arkitektur**
![Mikro service arkitektur](mikroservices-arkitektur.drawio.png)


Basert på kompleksiteten og fleksibiliteten til prosjektet, samt de definerte behovene og målene for PåGodVei-applikasjonen, ønsker vi å bruke mikrotjeneste arkitektur.

Begrunnelse:

1. Skalerbarhet: Mikrotjeneste-arkitektur tillater enklere skalerbarhet ved å kunne utvikle, deployere og skalere individuelle tjenester uavhengig av hverandre. Dette er spesielt viktig for et produkt som PåGodVei, som må kunne håndtere varierende mengder trafikk og data.

2. Fleksibilitet: Mikrotjeneste-arkitektur muliggjør løs kobling mellom tjenester, noe som gjør det lettere å implementere endringer og nye funksjoner uten å påvirke hele systemet. Dette er avgjørende i et prosjekt der behovene kan endre seg over tid, og hvor det er viktig å kunne tilpasse seg raskt.

3. Skalerbarhet: Med mikrotjeneste-arkitektur kan man optimalisere ressursbruk og ytelse ved å fordele oppgaver på flere små tjenester, i stedet for å ha alt samlet i én monolittisk applikasjon. Dette kan bidra til bedre ytelse og ressursutnyttelse, spesielt når det gjelder håndtering av sanntidsoppdateringer og brukerforespørsler.

4. Modulær utvikling: Mikrotjeneste-arkitektur legger til rette for modulær utvikling, der ulike deler av systemet kan utvikles og vedlikeholdes separat. Dette gjør det lettere å organisere utviklingsarbeidet og fremmer samtidig gjenbruk av kode og komponenter.

Samlet sett vil mikrotjeneste-arkitektur gi PåGodVei-applikasjonen den fleksibiliteten, skalerbarheten og modulære tilnærmingen som trengs for å møte prosjektets mål og utfordringer på en effektiv måte.

